import psycopg2


class Database:

    def __init__(self):
        self.conn = psycopg2.connect(host='postgres', database='signal',
                                     user='postgres', password='123456')

    def close(self):
        self.conn.close()

    def execute_sql_file(self, path_sql_file):
        with open(path_sql_file, "r") as query:
            for line in query:
                cursor = self.conn.cursor()
                cursor.execute(line)

        self.conn.commit()

    def load_initial_data(self):
        self.execute_sql_file("./sql/initial.sql")

    def clean_database(self):
        self.execute_sql_file("./sql/destroy.sql")

    def load_vrt_initial_data(self):
        self.execute_sql_file("sql/vrt-initial.sql")


class DatabaseVrt:

    def __init__(self):
        self.conn = psycopg2.connect(host='postgres', database='vrt_db',
                                     user='postgres', password='postgres')

    def close(self):
        self.conn.close()

    def execute_sql_file(self, path_sql_file):
        with open(path_sql_file, "r") as query:
            for line in query:
                cursor = self.conn.cursor()
                cursor.execute(line)

        self.conn.commit()

    def load_vrt_initial_data(self):
        self.execute_sql_file("./sql/vrt-initial.sql")

    def clean_vrt_database(self):
        self.execute_sql_file("./sql/vrt-destroy.sql")
