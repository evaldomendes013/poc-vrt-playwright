from database import DatabaseVrt
import variables
from playwright.sync_api import sync_playwright, expect
from visual_regression_tracker.playwright import PlaywrightVisualRegressionTracker, PageTrackOptions, \
    PageScreenshotOptions, Agent, ElementHandleScreenshotOptions, ElementHandleTrackOptions


def before_all(context):
    playwright = sync_playwright().start()
    browsertype = playwright.chromium
    browser = browsertype.launch(headless=True)
    context.contexto = browser.new_context()
    context.contexto.tracing.start(screenshots=True, snapshots=True, sources=True)
    context.page = context.contexto.new_page()
    context.expect = expect

    # database = Database()
    # database.load_initial_data()
    # database.close()

    # databasevrt = DatabaseVrt()
    # databasevrt.load_vrt_initial_data()
    # databasevrt.close()

    context.ElementHandleTrackOptions = ElementHandleTrackOptions
    context.ElementHandleScreenshotOptions = ElementHandleScreenshotOptions
    context.PageTrackOptions = PageTrackOptions
    context.Agent = Agent
    context.PageScreenshotOptions = PageScreenshotOptions
    context.vrt = PlaywrightVisualRegressionTracker(browsertype, variables.CONFIG)
    context.vrt.start()


def after_all(context):
    context.contexto.tracing.stop(path="./trace/trace.zip")

    # database = Database()
    # database.clean_database()
    # database.close()

    context.vrt.stop()
    context.page.close()
    print('passou aqui')
    # databasevrt = DatabaseVrt()
    # databasevrt.clean_vrt_database()
    # databasevrt.close()


def verify_if_element_equal(context, selector: str, name_image_base: str):
    context.vrt.trackElementHandle(context.page.query_selector(f"{selector}"),
                                   f"{name_image_base}", context.ElementHandleTrackOptions(
            diffTollerancePercent=0,
            screenshotOptions=context.ElementHandleScreenshotOptions(
                omit_background=True,
            ),
            agent=context.Agent(
                os='Linux',
                device='Desktop',
                viewport='1200x12'
            )
        )
    )
