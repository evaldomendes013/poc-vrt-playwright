# language: pt
Funcionalidade: Acesso ao google

  Contexto:
    Dado    que acesso o site na URL "https://www.google.com/"

  Cenário:  Botão pesquisar
    Então   o botão "Pesquisa Google" deve estar visível

  Cenário:  logo do google
    Então   o logo da google deve estar vísivel