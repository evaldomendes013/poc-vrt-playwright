import time

from behave import step
from features import environment


@step('que acesso o site na URL "{url}"')
def step_impl(context, url):
    context.page.set_viewport_size({"width": 1920, "height": 1080})
    context.page.goto(url)


@step('o botão "{botao}" deve estar visível')
def step_impl(context, botao):
    context.expect(context.page.locator("[name='q']")).to_be_visible()


@step("o logo da google deve estar vísivel")
def step_impl(context):
    # environment.verify_if_element_equal(context=context, selector='.lnXdpd', name_image_base='google')
    context.expect(context.page.locator("[name='q']")).to_be_visible()
